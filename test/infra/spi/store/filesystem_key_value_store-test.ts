import { expect } from "chai";

import { FilesystemKeyValueStore } from "infra/spi/store/filesystem_key_value_store";
import { TemporaryFilesystem } from "../../../helpers/temporary_filesystem";

describe("filesystem key/value store", () => {

  let filesystem: TemporaryFilesystem;

  before(() => {
    filesystem = TemporaryFilesystem.create();
  });

  after(() => {
    filesystem.destroy();
  });

  it("should retrieve all keys from store", () => {
    filesystem
      .withFile("04bc1c20-0833-11e9-ae6d-9721e8f6b101.todo")
      .withFile("92efee24-0831-11e9-8598-9b69d3cfecff.todo");
    expect(keys()).to.equal("04bc1c20-0833-11e9-ae6d-9721e8f6b101, 92efee24-0831-11e9-8598-9b69d3cfecff");
  });

  function keys(): string {
    return new FilesystemKeyValueStore(filesystem.base)
      .keys()
      .join(", ");
  }
});
