import { mkdtempSync, writeFileSync } from "fs";
import { List } from "immutable";
import { tmpdir } from "os";
import * as rimraf from "rimraf";
import { Maybe } from "tsmonad";

import { unindent } from "./unindent";

export class TemporaryFilesystem {
  public static create(): TemporaryFilesystem {
    return new TemporaryFilesystem(mkdtempSync(`${tmpdir()}/`, "utf8"));
  }

  private constructor(public readonly base: string) {
  }

  public withFile(name: string, content: string = ""): TemporaryFilesystem {
    writeFileSync(`${this.base}/${name}`, unindent(content), "utf8");
    return this;
  }

  public destroy() {
    rimraf(this.base, function() { /**/ });
  }
}
