import { List } from "immutable";

import { TodoListIdentifier } from "domain/model/todo_list_identifier";
import { TodoListIdentifierGenerator } from "domain/model/todo_list_identifier_generator";

export class FakeIdentifierGenerator implements TodoListIdentifierGenerator {
  private identifiers: List<TodoListIdentifier>;
  public constructor(...values: string[]) {
    this.identifiers = List(values.map((i) => TodoListIdentifier.fromString(i)));
  }

  public generate(): TodoListIdentifier {
    try {
      return this.identifiers.first();
    } finally {
      this.identifiers = this.identifiers.shift();
    }
  }
}
