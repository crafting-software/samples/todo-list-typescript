import { parseTodoLists, renderTodoLists } from "domain/model/todo_dsl";
import { TodoListStore } from "domain/model/todo_list_store";
import { TodoListMemoryStore } from "infra/spi/store/todo_list_memory_store";

export function storeWith(definition: string): TodoListStore {
  return new TodoListMemoryStore(parseTodoLists(definition));
}

export function renderStore(store: TodoListStore): string {
  return renderTodoLists(store.all());
}
