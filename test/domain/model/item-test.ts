import { expect } from "chai";

import { Item } from "domain/model/item";
import { ItemLabel } from "domain/model/item_label";
import { renderItem } from "domain/model/todo_dsl";

describe("item", () => {
  const item = Item.create(ItemLabel.of("item"));

  describe("check", () => {
    it("should be unchecked by default", () => {
      expect(renderItem(item)).to.equal("- [ ] item");
    });

    it("should set the check property to true", () => {
      expect(renderItem(item.check())).to.equal("- [x] item");
      expect(renderItem(item.check().check())).to.equal("- [x] item");
    });

    it("should make a copy", () => {
      item.check();
      expect(renderItem(item)).to.equal("- [ ] item");
    });
  });

  describe("uncheck", () => {
    it("should set the check property to false", () => {
      const checkedItem = item.check();
      expect(renderItem(checkedItem.uncheck())).to.equal("- [ ] item");
      expect(renderItem(checkedItem.uncheck().uncheck())).to.equal("- [ ] item");
    });
  });

  describe("toggle", () => {
    it("should set invert the check property", () => {
      expect(renderItem(item.toggle())).to.equal("- [x] item");
      expect(renderItem(item.toggle().toggle())).to.equal("- [ ] item");
    });
  });

  describe("edit label", () => {
    it("should change the label of the item", () => {
      expect(renderItem(item.edit(ItemLabel.of("paf")))).to.equal("- [ ] paf");
      expect(renderItem(item.check().edit(ItemLabel.of("paf")))).to.equal("- [x] paf");
    });
  });

});
