import { expect } from "chai";

import { parseTodoList, parseTodoLists, renderTodoList, renderTodoLists } from "domain/model/todo_dsl";
import { unindent } from "../../helpers/unindent";

describe("todo DSL should render and parse todo text representation", () => {

  it("should manage a list of todo list", () => {
    const content = unindent(`
      |(04bc1c20-0833-11e9-ae6d-9721e8f6b101) computers:
      |  - [x] Dell
      |  - [ ] Asus
      |  - [x] HP
      |  - [ ] Apple
      |
      |(92efee24-0831-11e9-8598-9b69d3cfecff) os:
      |  - [ ] Windows XP
      |  - [x] Debian
      |  - [x] Ubuntu
      |  - [ ] Redhat
      |  - [ ] MacOS
    `);

    expect(renderTodoLists(parseTodoLists(content))).to.equal(content);
  });

  it("should manage a single of todo list", () => {
    const content = unindent(`
      |(04bc1c20-0833-11e9-ae6d-9721e8f6b101) computers:
      |  - [x] Dell
      |  - [ ] Asus
      |  - [x] HP
      |  - [ ] Apple
    `);

    expect(renderTodoList(parseTodoList(content))).to.equal(content);
  });

});
