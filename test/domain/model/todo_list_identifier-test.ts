import { expect } from "chai";

import { TodoListIdentifier } from "domain/model/todo_list_identifier";

describe("todo list identifier", () => {

  describe("from string", () => {
    it("should take valid uuid as identifier", () => {
      expect(TodoListIdentifier.fromString("31dddd26-0c5f-11e9-be2c-13fadf007bf9").value).to.have.lengthOf(36);
    });

    it("should take hexadecimal with dash", () => {
      expect(TodoListIdentifier.fromString("2a-42").value).to.equal("2a-42");
    });

    it("should reject invalid uuid as identifier", () => {
      expect(() => TodoListIdentifier.fromString("invalid").value).to.throw(TypeError);
    });
  });

});
