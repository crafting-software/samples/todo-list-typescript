import { expect } from "chai";
import { Maybe } from "tsmonad";

import { Item } from "domain/model/item";
import { ItemIdentifier } from "domain/model/item_identifier";
import { ItemLabel } from "domain/model/item_label";
import { renderTodoList } from "domain/model/todo_dsl";
import { TodoList } from "domain/model/todo_list";
import { TodoListIdentifier } from "domain/model/todo_list_identifier";
import { TodoListName } from "domain/model/todo_list_name";
import { unindent } from "../../helpers/unindent";

describe("todo list", () => {
  const empty = TodoList.create(
    TodoListIdentifier.fromString("2a"),
    TodoListName.fromString("demo"));

  describe("empty list", () => {
    it("should have an identifier", () => {
      expect(empty.identifier.value).to.equal("2a");
    });

    it("should have a name", () => {
      expect(empty.name.value).to.equal("demo");
    });

    it("should have no item", () => {
      expect(empty.items.size).to.equal(0);
    });
  });

  describe("rename list", () => {
    it("should change the name of the list", () => {
      expect(empty.rename(TodoListName.fromString("new name")).name.value).to.equal("new name");
    });
  });

  describe("create item", () => {
    it("should be able to create new item", () => {
      expect(renderTodoList(empty.createItem(ItemLabel.of("first")))).to.equal(unindent(`
        |(2a) demo:
        |  - [ ] first
      `));
    });

    it("new item should be unckecked", () => {
      expect(valueOf(empty.createItem(ItemLabel.of("first")).item(ItemIdentifier.of(0))).isChecked).to.equal(false);
    });
  });

  describe("get item", () => {
    it("should return nothing when requesting item with out of range index", () => {
      expect(Maybe.isNothing(empty.item(ItemIdentifier.of(42)))).to.equal(true);
    });

    it("should return item when requesting item with correct index", () => {
      const todo = empty.createItem(ItemLabel.of("first")).createItem(ItemLabel.of("second"));
      expect(valueOf(todo.item(ItemIdentifier.of(0))).label.value).to.equal("first");
      expect(valueOf(todo.item(ItemIdentifier.of(1))).label.value).to.equal("second");
    });
  });

  describe("delete item", () => {
    it("should do nothing when delete index out of range", () => {
      expect(renderTodoList(empty.createItem(ItemLabel.of("first")).delete(ItemIdentifier.of(42)))).to.equal(unindent(`
        |(2a) demo:
        |  - [ ] first
      `));
    });

    it("should delete item with correct index", () => {
      expect(renderTodoList(empty.createItem(ItemLabel.of("first")).delete(ItemIdentifier.of(0))))
        .to.equal(unindent(`
          |(2a) demo:
          |  <empty>
        `));
      expect(renderTodoList(empty
        .createItem(ItemLabel.of("first"))
        .createItem(ItemLabel.of("second"))
        .delete(ItemIdentifier.of(0))))
        .to.equal(unindent(`
          |(2a) demo:
          |  - [ ] second
        `));
    });
  });

  describe("item action", () => {
    it("should do nothing when index out of range", () => {
      expect(renderTodoList(empty.createItem(ItemLabel.of("first")).updateItem(ItemIdentifier.of(42), Item.check())))
        .to.equal(unindent(`
          |(2a) demo:
          |  - [ ] first
        `));
    });

    it("should perform action with correct item index", () => {
      expect(renderTodoList(empty.createItem(ItemLabel.of("first")).updateItem(ItemIdentifier.of(0), Item.toggle())))
        .to.equal(unindent(`
          |(2a) demo:
          |  - [x] first
        `));
    });
  });

  function valueOf(maybe: Maybe<Item>): Item {
    return maybe.valueOr(null);
  }
});
