import { expect } from "chai";

import { ItemIdentifier } from "domain/model/item_identifier";

describe("item identifier", () => {

  describe("from number", () => {
    it("should take a positive integer as identifier", () => {
      expect(ItemIdentifier.of(0).value).to.equal(0);
      expect(ItemIdentifier.of(42).value).to.equal(42);
    });

    it("should reject negative number as identifier", () => {
      expect(() => ItemIdentifier.of(-1)).to.throw(TypeError);
    });
  });

});
