import { expect } from "chai";

import { scenario } from "./scenario";

describe("usecases", () => {

  describe("list managment", () => {

    it("should create a todo list", () => {
      scenario()
        .given_empty_store()
        .and_next_identifiers_are("2a")
        .when_I_create_a_new_list_named("my list")
        .then_store_should_contains(`
          |(2a) my list:
          |  <empty>
          `);
    });

    it("should list all todo lists", () => {
      scenario()
        .given_store(`
          |(01) first todo list:
          |  - [x] first
          |
          |(02) second todo list:
          |  - [x] first
        `)
        .then_the_list_all_todo_lists_should_be("first todo list, second todo list");
    });

    it("should rename a todo list", () => {
      scenario()
        .given_store(`
          |(2a) list:
          |  <empty>
        `)
        .when_I_rename_the_list("2a", "new name")
        .then_store_should_contains(`
          |(2a) new name:
          |  <empty>
          `);
    });

    it("should delete a todo list", () => {
      scenario()
        .given_store(`
          |(2a) list:
          |  <empty>
        `)
        .when_I_delete_list_identified_by("2a")
        .then_store_should_be_empty();
    });

  });

});
