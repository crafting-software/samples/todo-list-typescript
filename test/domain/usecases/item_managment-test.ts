import { expect } from "chai";

import { scenario } from "./scenario";

describe("usecases", () => {

    describe("item managment", () => {

    it("create should add an new item on existing list", () => {
      scenario()
        .given_store(`
          |(2a) my list:
          |  <empty>
        `)
        .when_I_create_item_on_list("2a", "first")
        .then_store_should_contains(`
          |(2a) my list:
          |  - [ ] first
          `);
    });

    it("create should do nothing on unknown list", () => {
      scenario()
        .given_empty_store()
        .when_I_create_item_on_list("2a", "first")
        .then_store_should_be_empty();
    });

    it("edit should change the item label", () => {
      scenario()
        .given_store(`
          |(2a) my list:
          |  - [ ] item
        `)
        .when_I_edit_item_on_list("2a", 0, "edited")
        .then_store_should_contains(`
          |(2a) my list:
          |  - [ ] edited
          `);
    });

    it("check should change the status of an unchecked item", () => {
      scenario()
        .given_store(`
          |(2a) my list:
          |  - [ ] first
        `)
        .when_I_check_item("2a", 0)
        .then_store_should_contains(`
          |(2a) my list:
          |  - [x] first
          `);
    });

    it("check should do nothing on already checked item", () => {
      scenario()
        .given_store(`
          |(2a) my list:
          |  - [x] first
        `)
        .when_I_check_item("2a", 0)
        .then_store_should_contains(`
          |(2a) my list:
          |  - [x] first
          `);
    });

    it("check should do nothing on unknown item", () => {
      scenario()
        .given_store(`
          |(2a) my list:
          |  - [ ] first
        `)
        .when_I_check_item("2a", 5)
        .then_store_should_contains(`
          |(2a) my list:
          |  - [ ] first
          `);
    });

    it("check should do nothing on unknown list", () => {
      scenario()
        .given_empty_store()
        .when_I_check_item("2a", 0)
        .then_store_should_be_empty();
    });

    it("toggle should check an uncheked item", () => {
      scenario()
        .given_store(`
          |(2a) my list:
          |  - [ ] first
        `)
        .when_I_toggle_item("2a", 0)
        .then_store_should_contains(`
          |(2a) my list:
          |  - [x] first
          `);
    });

    it("toggle should uncheck a checked item", () => {
      scenario()
        .given_store(`
          |(2a) my list:
          |  - [x] first
        `)
        .when_I_toggle_item("2a", 0)
        .then_store_should_contains(`
          |(2a) my list:
          |  - [ ] first
          `);
    });

    it("toggle should do nothing on unknown item", () => {
      scenario()
        .given_store(`
          |(2a) my list:
          |  - [x] first
        `)
        .when_I_toggle_item("2a", 5)
        .then_store_should_contains(`
          |(2a) my list:
          |  - [x] first
          `);
    });

    it("toggle should do nothing on unknown list", () => {
      scenario()
        .given_empty_store()
        .when_I_toggle_item("2a", 0)
        .then_store_should_be_empty();
    });

    it("uncheck should change the status of a checked item", () => {
      scenario()
        .given_store(`
          |(2a) my list:
          |  - [x] first
        `)
        .when_I_uncheck_item("2a", 0)
        .then_store_should_contains(`
          |(2a) my list:
          |  - [ ] first
          `);
    });

    it("uncheck should do nothing on unchecked item", () => {
      scenario()
        .given_store(`
          |(2a) my list:
          |  - [ ] first
        `)
        .when_I_uncheck_item("2a", 0)
        .then_store_should_contains(`
          |(2a) my list:
          |  - [ ] first
          `);
    });

    it("uncheck should do nothing on unknown item", () => {
      scenario()
        .given_store(`
          |(2a) my list:
          |  - [x] first
        `)
        .when_I_uncheck_item("2a", 5)
        .then_store_should_contains(`
          |(2a) my list:
          |  - [x] first
          `);
    });

    it("uncheck should do nothing on unknown list", () => {
      scenario()
        .given_empty_store()
        .when_I_uncheck_item("2a", 0)
        .then_store_should_be_empty();
    });

    it("delete should remove an existing item", () => {
      scenario()
        .given_store(`
          |(2a) my list:
          |  - [x] first
        `)
        .when_I_delete_item("2a", 0)
        .then_store_should_contains(`
          |(2a) my list:
          |  <empty>
          `);
    });

    it("delete should do nothing on unknwon item", () => {
      scenario()
        .given_store(`
          |(2a) my list:
          |  - [x] first
        `)
        .when_I_delete_item("2a", 5)
        .then_store_should_contains(`
          |(2a) my list:
          |  - [x] first
          `);
    });

    it("delete should do nothing on unknown list", () => {
      scenario()
        .given_empty_store()
        .when_I_delete_item("2a", 0)
        .then_store_should_be_empty();
    });

  });

});
