import { expect } from "chai";

import { ItemIdentifier } from "domain/model/item_identifier";
import { ItemLabel } from "domain/model/item_label";
import { TodoListIdentifier } from "domain/model/todo_list_identifier";
import { TodoListIdentifierGenerator } from "domain/model/todo_list_identifier_generator";
import { TodoListName } from "domain/model/todo_list_name";
import { TodoListStore } from "domain/model/todo_list_store";
import { AllLists } from "domain/usecases/all_lists";
import { CheckItem } from "domain/usecases/check_item";
import { CreateItem } from "domain/usecases/create_item";
import { CreateList } from "domain/usecases/create_list";
import { DeleteItem } from "domain/usecases/delete_item";
import { DeleteList } from "domain/usecases/delete_list";
import { EditItem } from "domain/usecases/edit_item";
import { RenameList } from "domain/usecases/rename_list";
import { ToggleItem } from "domain/usecases/toggle_item";
import { UncheckItem } from "domain/usecases/uncheck_item";
import { FakeIdentifierGenerator } from "../../helpers/fake_identifier_generator";
import { renderStore, storeWith } from "../../helpers/store_dsl";
import { unindent } from "../../helpers/unindent";

export function scenario(): Scenario {
  return new Scenario();
}

class Scenario {
  private store: TodoListStore = storeWith("");
  private generator: TodoListIdentifierGenerator = new FakeIdentifierGenerator();

  public given_store(content: string): Scenario {
    this.store = storeWith(unindent(content));
    return this;
  }

  public given_empty_store(): Scenario {
    this.store = storeWith("");
    return this;
  }

  public and_next_identifiers_are(...values: string[]): Scenario {
    this.generator = new FakeIdentifierGenerator(...values);
    return this;
  }

  public when_I_create_a_new_list_named(name: string): Scenario {
    new CreateList(this.store, this.generator).createList(TodoListName.fromString(name));
    return this;
  }

  public when_I_rename_the_list(identifier: string, newName: string): Scenario {
    new RenameList(this.store).renameList(
      TodoListIdentifier.fromString(identifier),
      TodoListName.fromString(newName));
    return this;
  }

  public when_I_delete_list_identified_by(identifier: string): Scenario {
    new DeleteList(this.store).deleteList(TodoListIdentifier.fromString(identifier));
    return this;
  }

  public when_I_create_item_on_list(identifier: string, label: string): Scenario {
    new CreateItem(this.store).createItem(TodoListIdentifier.fromString(identifier), ItemLabel.of(label));
    return this;
  }

  public when_I_edit_item_on_list(identifier: string, index: number, label: string): Scenario {
    new EditItem(this.store)
      .editItem(TodoListIdentifier.fromString(identifier), ItemIdentifier.of(index), ItemLabel.of(label));
    return this;
  }

  public when_I_check_item(identifier: string, index: number): Scenario {
    new CheckItem(this.store).checkItem(TodoListIdentifier.fromString(identifier), ItemIdentifier.of(index));
    return this;
  }

  public when_I_toggle_item(identifier: string, index: number): Scenario {
    new ToggleItem(this.store).toggleItem(TodoListIdentifier.fromString(identifier), ItemIdentifier.of(index));
    return this;
  }

  public when_I_uncheck_item(identifier: string, index: number): Scenario {
    new UncheckItem(this.store).uncheckItem(TodoListIdentifier.fromString(identifier), ItemIdentifier.of(index));
    return this;
  }

  public when_I_delete_item(identifier: string, index: number): Scenario {
    new DeleteItem(this.store).deleteItem(TodoListIdentifier.fromString(identifier), ItemIdentifier.of(index));
    return this;
  }

  public then_the_list_all_todo_lists_should_be(expected: string): Scenario {
    expect(new AllLists(this.store).all()
      .map((list) => list.name.value)
      .join(", "))
      .to.equal(expected);
    return this;
  }

  public then_store_should_be_empty(): Scenario {
    expect(this.store.all().size).to.equal(0);
    return this;
  }

  public then_store_should_contains(content: string): Scenario {
    expect(renderStore(this.store)).to.equal(unindent(content));
    return this;
  }

}
