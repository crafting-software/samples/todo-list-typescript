import { v4 as uuid } from "uuid";

import { TodoListIdentifier } from "domain/model/todo_list_identifier";
import { TodoListIdentifierGenerator } from "domain/model/todo_list_identifier_generator";

export class TodoListIdentifierUuidGenerator implements TodoListIdentifierGenerator {
  public generate(): TodoListIdentifier {
    return TodoListIdentifier.fromString(uuid());
  }
}
