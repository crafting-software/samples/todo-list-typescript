import { readdirSync, readFileSync, unlinkSync, writeFileSync } from "fs";
import { List } from "immutable";
import { Maybe } from "tsmonad";

import { Key, KeyValueStore, Value } from "infra/spi/store/key_value_store";

export class FilesystemKeyValueStore implements KeyValueStore {
  public constructor(public readonly path: string) {
  }

  public save(key: Key, value: Value) {
    writeFileSync(this.keyToFilename(key), value, "utf8");
  }

  public load(key: Key): Maybe<Value> {
    return Maybe.maybe(
      this.keys()
        .filter((k) => k === key)
        .map((k) => this.keyToFilename(k))
        .map((filename) => readFileSync(filename, "utf8"))
        .first());
  }

  public keys(): List<Key> {
    return List(
      readdirSync(this.path, {withFileTypes: true})
        .filter((dirent) => dirent.isFile())
        .map((dirent) => dirent.name)
        .filter((filename) => filename.match(/^.*\.todo$/g))
        .map((filename) => this.filenameToKey(filename)));
  }

  public values(): List<Value> {
    return this.keys()
      .map((key) => this.keyToFilename(key))
      .map((filename) => readFileSync(filename, "utf8"));
  }

  public remove(key: Key) {
    unlinkSync(this.keyToFilename(key));
  }

  private keyToFilename(key: Key): string {
    return `${this.path}/${key}.todo`;
  }

  private filenameToKey(filename: string): Key {
    return filename.replace(/\.todo$/g, "");
  }
}
