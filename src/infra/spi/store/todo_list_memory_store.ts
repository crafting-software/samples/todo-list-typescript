import { List, Map } from "immutable";
import { Maybe } from "tsmonad";

import { TodoList } from "domain/model/todo_list";
import { TodoListIdentifier } from "domain/model/todo_list_identifier";
import { TodoListStore } from "domain/model/todo_list_store";

export class TodoListMemoryStore implements TodoListStore {
  private entries: Map<string, TodoList> = Map();

  public constructor(lists: List<TodoList> = List()) {
    lists.forEach((list) => this.save(list));
  }

  public all(): List<TodoList> {
    return this.entries.toList();
  }

  public findById(identifier: TodoListIdentifier): Maybe<TodoList> {
    if (this.entries.has(identifier.value)) {
      return Maybe.just(this.entries.get(identifier.value));
    }
    return Maybe.nothing();
  }

  public save(list: TodoList): TodoList {
    this.entries = this.entries.set(list.identifier.value, list);
    return list;
  }

  public delete(identifier: TodoListIdentifier): void {
    this.entries = this.entries.delete(identifier.value);
  }
}
