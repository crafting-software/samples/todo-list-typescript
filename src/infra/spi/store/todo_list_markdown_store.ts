import { readdirSync, readFileSync, unlinkSync, writeFileSync } from "fs";
import { List } from "immutable";
import { Maybe } from "tsmonad";

import { parseTodoList, renderTodoList } from "domain/model/todo_dsl";
import { TodoList } from "domain/model/todo_list";
import { TodoListIdentifier } from "domain/model/todo_list_identifier";
import { TodoListStore } from "domain/model/todo_list_store";
import { FilesystemKeyValueStore } from "infra/spi/store/filesystem_key_value_store";
import { KeyValueStore } from "infra/spi/store/key_value_store";

export function markdownStore(path: string): TodoListStore {
  return new TodoListMarkdownStore(new FilesystemKeyValueStore(path));
}

class TodoListMarkdownStore implements TodoListStore {
  public constructor(public readonly physical: KeyValueStore) {
  }

  public all(): List<TodoList> {
    return this.physical.values().map((content) => parseTodoList(content));
  }

  public findById(identifier: TodoListIdentifier): Maybe<TodoList> {
    return this.physical.load(identifier.value).map((content) => parseTodoList(content));
  }

  public save(list: TodoList): TodoList {
    this.physical.save(list.identifier.value, renderTodoList(list));
    return list;
  }

  public delete(identifier: TodoListIdentifier): void {
    this.physical.remove(identifier.value);
  }
}
