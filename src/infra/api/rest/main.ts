/* tslint:disable:no-console */
const Help = `
Usage:
  api [options]
  api -h | --help
  api --version

Options:
  -h --help     Show this screen.
  --version     Show version.
  --port=P      The port to listen to [default: 3000].
`;

import { docopt } from "docopt";
import * as express from "express";
import * as morgan from "morgan";

import { routes } from "infra/api/rest/routes";

export function runServer(port: number): void {
  const app = express();
  app.use(express.json());
  morgan.token("body", (req, res) => JSON.stringify(req.body));
  app.use(morgan(":method :url :status :response-time ms - :res[content-length] - body :body"));

  routes(app).listen(port, () => console.log(`Server listening on port ${port}!`));
}

const options = docopt(Help, {version: "0.1"});
runServer(options["--port"]);
