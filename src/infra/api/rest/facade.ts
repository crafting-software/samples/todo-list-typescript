import * as express from "express";
import { List } from "immutable";
import { Maybe } from "tsmonad";

import { ItemIdentifier } from "domain/model/item_identifier";
import { ItemLabel } from "domain/model/item_label";
import { TodoList } from "domain/model/todo_list";
import { TodoListIdentifier } from "domain/model/todo_list_identifier";
import { TodoListName } from "domain/model/todo_list_name";
import { TodoListStore } from "domain/model/todo_list_store";
import { AllLists } from "domain/usecases/all_lists";
import { CreateItem } from "domain/usecases/create_item";
import { CreateList } from "domain/usecases/create_list";
import { DeleteItem } from "domain/usecases/delete_item";
import { DeleteList } from "domain/usecases/delete_list";
import { EditItem } from "domain/usecases/edit_item";
import { RenameList } from "domain/usecases/rename_list";
import { ToggleItem } from "domain/usecases/toggle_item";
import { renderTodoList, renderTodoLists } from "infra/api/rest/json_codec";
import { markdownStore } from "infra/spi/store/todo_list_markdown_store";
import { TodoListIdentifierUuidGenerator } from "infra/spi/todo_list_identifier_uuid_generator";

export class RestFacade {
  public constructor(private readonly store: TodoListStore) {
  }

  public createList(): express.RequestHandler {
    return (request, response) =>
      replySingleList(
        response,
        new CreateList(this.store, new TodoListIdentifierUuidGenerator())
          .createList(listName(request)));
  }

  public all(): express.RequestHandler {
    return (request, response) => replyMultipleLists( response, new AllLists(this.store).all());
  }

  public getList(): express.RequestHandler {
    return (request, response) => replyMaybeList(
      response,
      this.store.findById(listId(request)));
  }

  public renameList(): express.RequestHandler {
    return (request, response) => replyMaybeList(
      response,
      new RenameList(this.store).renameList(listId(request), listName(request)));
  }

  public deleteList(): express.RequestHandler {
    return (request, response) => {
      new DeleteList(this.store).deleteList(listId(request));
      return {};
    };
  }

  public createItem(): express.RequestHandler {
    return (request, response) => replyMaybeList(
      response,
      new CreateItem(this.store).createItem(listId(request), itemLabel(request)));
  }

  public editItem(): express.RequestHandler {
    return (request, response) => replyMaybeList(
      response,
      new EditItem(this.store).editItem(listId(request), itemId(request), itemLabel(request)));
  }

  public deleteItem(): express.RequestHandler {
    return (request, response) => replyMaybeList(
      response,
      new DeleteItem(this.store).deleteItem(listId(request), itemId(request)));
  }

  public toggleItem(): express.RequestHandler {
    return (request, response) => replyMaybeList(
      response,
      new ToggleItem(this.store).toggleItem(listId(request), itemId(request)));
  }
}

function replySingleList(response: express.Response, list: TodoList): void {
  response.json(renderTodoList(list));
}

function replyMaybeList(response: express.Response, list: Maybe<TodoList>): void {
  response.json(list.map(renderTodoList).valueOr({}));
}

function replyMultipleLists(response: express.Response, lists: List<TodoList>): void {
  response.json(renderTodoLists(lists));
}

function listId(request: express.Request): TodoListIdentifier {
  return TodoListIdentifier.fromString(request.params.listId);
}

function listName(request: express.Request): TodoListName {
  return TodoListName.fromString(request.body.name);
}

function itemLabel(request: express.Request): ItemLabel {
  return ItemLabel.of(request.body.label);
}

function itemId(request: express.Request): ItemIdentifier {
  return ItemIdentifier.of(request.params.itemId);
}
