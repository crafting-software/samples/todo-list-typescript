import { List } from "immutable";

import { Item } from "domain/model/item";
import { ItemLabel } from "domain/model/item_label";
import { TodoList } from "domain/model/todo_list";
import { TodoListIdentifier } from "domain/model/todo_list_identifier";
import { TodoListName } from "domain/model/todo_list_name";

export function renderTodoLists(lists: List<TodoList>): any {
  return lists.map((list) => renderTodoList(list));
}

export function renderTodoList(list: TodoList): any {
  return { identifier: list.identifier.value, name: list.name.value, items: renderItems(list.items) };
}

export function renderItems(items: List<Item>): any {
  return items.map((item) => renderItem(item));
}

export function renderItem(item: Item): any {
  return { label: item.label.value, checked: item.isChecked };
}
