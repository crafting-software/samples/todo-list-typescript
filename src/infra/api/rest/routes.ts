import * as express from "express";

import { RestFacade } from "infra/api/rest/facade";
import { markdownStore } from "infra/spi/store/todo_list_markdown_store";

export function routes(app: express.Express): express.Express {
  const facade = new RestFacade(markdownStore("./demo/db/"));
  app.post("/list", facade.createList());
  app.get("/list", facade.all());
  app.get("/list/:listId", facade.getList());
  app.delete("/list/:listId", facade.deleteList());
  app.post("/list/:listId/name", facade.renameList());
  app.post("/list/:listId/item", facade.createItem());
  app.post("/list/:listId/item/:itemId/label", facade.editItem());
  app.delete("/list/:listId/item/:itemId", facade.deleteItem());
  app.post("/list/:listId/item/:itemId/toggle", facade.toggleItem());
  return app;
}
