/* tslint:disable:no-console */
const Help = `
Usage:
  cli list create <name>
  cli list all
  cli list <id> rename <name>
  cli list <id> delete
  cli item create <id> <label>
  cli item edit <id> <index> <label>
  cli item delete <id> <index>
  cli item toggle <id> <index>
  cli -h | --help
  cli --version

Options:
  -h --help     Show this screen.
  --version     Show version.
`;

import { docopt } from "docopt";

import { CliFacade } from "infra/api/cli/facade";
import { markdownStore } from "infra/spi/store/todo_list_markdown_store";

const options = docopt(Help, {version: "0.1"});

const facade = new CliFacade(markdownStore("./demo/db/"));

if (options.list) {
  if (options.create) {
    console.log(facade.createList(options["<name>"]));
  } else if (options.all) {
    console.log(facade.all());
  } else if (options.rename) {
    console.log(facade.renameList(options["<id>"], options["<name>"]));
  } else if (options.delete) {
    console.log(facade.deleteList(options["<id>"]));
  }
} else if (options.item) {
  if (options.create) {
    console.log(facade.createItem(options["<id>"], options["<label>"]));
  } else if (options.edit) {
    console.log(facade.editItem(options["<id>"], options["<index>"], options["<label>"]));
  } else if (options.delete) {
    console.log(facade.deleteItem(options["<id>"], options["<index>"]));
  } else if (options.toggle) {
    console.log(facade.toggleItem(options["<id>"], options["<index>"]));
  }
}
