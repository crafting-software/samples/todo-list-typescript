import { Maybe } from "tsmonad";

import { ItemIdentifier } from "domain/model/item_identifier";
import { ItemLabel } from "domain/model/item_label";
import { renderTodoList, renderTodoLists } from "domain/model/todo_dsl";
import { TodoList } from "domain/model/todo_list";
import { TodoListIdentifier } from "domain/model/todo_list_identifier";
import { TodoListName } from "domain/model/todo_list_name";
import { TodoListStore } from "domain/model/todo_list_store";
import { AllLists } from "domain/usecases/all_lists";
import { CreateItem } from "domain/usecases/create_item";
import { CreateList } from "domain/usecases/create_list";
import { DeleteItem } from "domain/usecases/delete_item";
import { DeleteList } from "domain/usecases/delete_list";
import { EditItem } from "domain/usecases/edit_item";
import { RenameList } from "domain/usecases/rename_list";
import { ToggleItem } from "domain/usecases/toggle_item";
import { TodoListIdentifierUuidGenerator } from "infra/spi/todo_list_identifier_uuid_generator";

export class CliFacade {
  public constructor(private readonly store: TodoListStore) {
  }

  public createList(name: string): string {
    return renderTodoList(
      new CreateList(this.store, new TodoListIdentifierUuidGenerator())
        .createList(TodoListName.fromString(name)));
  }

  public all(): string {
    return renderTodoLists(new AllLists(this.store).all());
  }

  public renameList(identifier: string, name: string): string {
    return renderList(new RenameList(this.store)
      .renameList(TodoListIdentifier.fromString(identifier), TodoListName.fromString(name)));
  }

  public deleteList(identifier: string): string {
    new DeleteList(this.store).deleteList(TodoListIdentifier.fromString(identifier));
    return "";
  }

  public createItem(identifier: string, label: string): string {
    return renderList(new CreateItem(this.store)
      .createItem(TodoListIdentifier.fromString(identifier), ItemLabel.of(label)));
  }

  public editItem(identifier: string, index: number, label: string): string {
    return renderList(new EditItem(this.store)
      .editItem(TodoListIdentifier.fromString(identifier), ItemIdentifier.of(index), ItemLabel.of(label)));
  }

  public deleteItem(identifier: string, index: number): string {
    return renderList(new DeleteItem(this.store)
      .deleteItem(TodoListIdentifier.fromString(identifier), ItemIdentifier.of(index)));
  }

  public toggleItem(identifier: string, index: number): string {
    return renderList(new ToggleItem(this.store)
      .toggleItem(TodoListIdentifier.fromString(identifier), ItemIdentifier.of(index)));
  }
}

function renderList(list: Maybe<TodoList>): string {
  return list.map(renderTodoList).valueOr("Nothing done :(");
}
