import { TodoList } from "domain/model/todo_list";
import { TodoListIdentifierGenerator } from "domain/model/todo_list_identifier_generator";
import { TodoListName } from "domain/model/todo_list_name";
import { TodoListStore } from "domain/model/todo_list_store";

export class CreateList {
  public constructor(private readonly store: TodoListStore, private readonly generator: TodoListIdentifierGenerator) {
  }

  public createList(name: TodoListName): TodoList {
    return this.store.save(TodoList.create(this.generator.generate(), name));
  }
}
