import { Maybe } from "tsmonad";

import { ItemLabel } from "domain/model/item_label";
import { TodoList } from "domain/model/todo_list";
import { TodoListIdentifier } from "domain/model/todo_list_identifier";
import { TodoListStore } from "domain/model/todo_list_store";
import { updateList } from "domain/usecases/util_update_list";

export class CreateItem {
  public constructor(private readonly store: TodoListStore) {
  }

  public createItem(listIdentifier: TodoListIdentifier, label: ItemLabel): Maybe<TodoList> {
    return updateList(this.store, listIdentifier, (list) => list.createItem(label));
  }
}
