import { List } from "immutable";

import { TodoList } from "domain/model/todo_list";
import { TodoListStore } from "domain/model/todo_list_store";

export class AllLists {
  public constructor(private readonly store: TodoListStore) {
  }

  public all(): List<TodoList> {
    return this.store.all();
  }
}
