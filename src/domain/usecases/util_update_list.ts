import { Maybe } from "tsmonad";

import { ItemIdentifier } from "domain/model/item_identifier";
import { TodoList } from "domain/model/todo_list";
import { TodoListIdentifier } from "domain/model/todo_list_identifier";
import { TodoListStore } from "domain/model/todo_list_store";

export function updateList(
  store: TodoListStore,
  listIdentifier: TodoListIdentifier,
  functor: (list: TodoList) => TodoList): Maybe<TodoList> {
  return store
    .findById(listIdentifier)
    .map(functor)
    .do({just: (list) => store.save(list)});
}
