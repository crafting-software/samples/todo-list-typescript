import { TodoListIdentifier } from "domain/model/todo_list_identifier";
import { TodoListStore } from "domain/model/todo_list_store";

export class DeleteList {
  public constructor(private readonly store: TodoListStore) {
  }

  public deleteList(identifier: TodoListIdentifier): void {
    this.store.delete(identifier);
  }
}
