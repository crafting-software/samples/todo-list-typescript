import { Maybe } from "tsmonad";

import { Item } from "domain/model/item";
import { ItemIdentifier } from "domain/model/item_identifier";
import { TodoList } from "domain/model/todo_list";
import { TodoListIdentifier } from "domain/model/todo_list_identifier";
import { TodoListStore } from "domain/model/todo_list_store";
import { updateList } from "domain/usecases/util_update_list";

export class UncheckItem {
  public constructor(private readonly store: TodoListStore) {
  }

  public uncheckItem(listIdentifier: TodoListIdentifier, itemIdentifier: ItemIdentifier): Maybe<TodoList> {
    return updateList(this.store, listIdentifier, (list) => list.updateItem(itemIdentifier, Item.uncheck()));
  }
}
