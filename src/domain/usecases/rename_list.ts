import { Maybe } from "tsmonad";

import { TodoList } from "domain/model/todo_list";
import { TodoListIdentifier } from "domain/model/todo_list_identifier";
import { TodoListName } from "domain/model/todo_list_name";
import { TodoListStore } from "domain/model/todo_list_store";
import { updateList } from "domain/usecases/util_update_list";

export class RenameList {
  public constructor(private readonly store: TodoListStore) {
  }

  public renameList(identifier: TodoListIdentifier, newName: TodoListName): Maybe<TodoList> {
    return updateList(this.store, identifier, (list) => list.rename(newName));
  }
}
