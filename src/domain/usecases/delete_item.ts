import { Maybe } from "tsmonad";

import { ItemIdentifier } from "domain/model/item_identifier";
import { TodoList } from "domain/model/todo_list";
import { TodoListIdentifier } from "domain/model/todo_list_identifier";
import { TodoListStore } from "domain/model/todo_list_store";
import { updateList } from "domain/usecases/util_update_list";

export class DeleteItem {
  public constructor(private readonly store: TodoListStore) {
  }

  public deleteItem(listIdentifier: TodoListIdentifier, itemIdentifier: ItemIdentifier): Maybe<TodoList> {
    return updateList(this.store, listIdentifier, (list) => list.delete(itemIdentifier));
  }
}
