import { Maybe } from "tsmonad";

import { Item } from "domain/model/item";
import { ItemIdentifier } from "domain/model/item_identifier";
import { ItemLabel } from "domain/model/item_label";
import { TodoList } from "domain/model/todo_list";
import { TodoListIdentifier } from "domain/model/todo_list_identifier";
import { TodoListStore } from "domain/model/todo_list_store";
import { updateList } from "domain/usecases/util_update_list";

export class EditItem {
  public constructor(private readonly store: TodoListStore) {
  }

  public editItem(
    listIdentifier: TodoListIdentifier,
    itemIdentifier: ItemIdentifier,
    label: ItemLabel): Maybe<TodoList> {
      return updateList(this.store, listIdentifier, (list) => list.updateItem(itemIdentifier, Item.edit(label)));
  }
}
