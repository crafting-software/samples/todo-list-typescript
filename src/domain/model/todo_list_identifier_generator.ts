import { TodoListIdentifier } from "domain/model/todo_list_identifier";

export interface TodoListIdentifierGenerator {
  generate(): TodoListIdentifier;
}
