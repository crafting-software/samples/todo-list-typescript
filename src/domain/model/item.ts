import { ItemLabel } from "domain/model/item_label";

export type ItemAction = (item: Item) => Item;

export class Item {
  public static edit(label: ItemLabel): ItemAction {
    return (item) => item.edit(label);
  }

  public static check(): ItemAction {
    return (item) => item.check();
  }

  public static uncheck(): ItemAction {
    return (item) => item.uncheck();
  }

  public static toggle(): ItemAction {
    return (item) => item.toggle();
  }

  public static create(label: ItemLabel): Item {
    return new Item(label, false);
  }

  private constructor(public readonly label: ItemLabel, public readonly isChecked: boolean) {
  }

  public edit(label: ItemLabel): Item {
    return new Item(label, this.isChecked);
  }

  public check(): Item {
    return new Item(this.label, true);
  }

  public uncheck(): Item {
    return new Item(this.label, false);
  }

  public toggle(): Item {
    return new Item(this.label, !this.isChecked);
  }
}
