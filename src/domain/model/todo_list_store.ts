import { List, Map } from "immutable";
import { Maybe } from "tsmonad";

import { TodoList } from "domain/model/todo_list";
import { TodoListIdentifier } from "domain/model/todo_list_identifier";

export interface TodoListStore {
  all(): List<TodoList>;
  findById(identifier: TodoListIdentifier): Maybe<TodoList>;
  save(list: TodoList): TodoList;
  delete(identifier: TodoListIdentifier): void;
}
