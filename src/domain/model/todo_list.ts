import { List } from "immutable";
import { Maybe } from "tsmonad";

import { Item, ItemAction } from "domain/model/item";
import { ItemIdentifier } from "domain/model/item_identifier";
import { ItemLabel } from "domain/model/item_label";
import { TodoListIdentifier } from "domain/model/todo_list_identifier";
import { TodoListName } from "domain/model/todo_list_name";

export class TodoList {
  public static create(identifier: TodoListIdentifier, name: TodoListName): TodoList {
    return new TodoList(identifier, name, List());
  }

  private constructor(
    public readonly identifier: TodoListIdentifier,
    public readonly name: TodoListName,
    public readonly items: List<Item>) {
  }

  public rename(name: TodoListName): TodoList {
    return new TodoList(this.identifier, name, this.items);
  }

  public createItem(label: ItemLabel): TodoList {
    return this.withItems(this.items.push(Item.create(label)));
  }

  public item(itemIdentifier: ItemIdentifier): Maybe<Item> {
    return this.index(itemIdentifier).map((index) => this.items.get(index));
  }

  public delete(itemIdentifier: ItemIdentifier): TodoList {
    return this.onIndex(itemIdentifier, (index) => this.items.delete(index));
  }

  public updateItem(itemIdentifier: ItemIdentifier, functor: ItemAction): TodoList {
    return this.onIndex(itemIdentifier, (index) => this.items.update(index, functor));
  }

  public withItems(items: List<Item>): TodoList {
    if (items.equals(this.items)) {
      return this;
    }
    return new TodoList(this.identifier, this.name, items);
  }

  private index(itemIdentifier: ItemIdentifier): Maybe<number> {
    if (itemIdentifier.value < 0 || itemIdentifier.value >= this.items.size) {
      return Maybe.nothing();
    }
    return Maybe.just(itemIdentifier.value);
  }

  private onIndex(itemIdentifier: ItemIdentifier, functor: (index: number) => List<Item>): TodoList {
    return this.withItems(this.index(itemIdentifier).map(functor).valueOr(this.items));
  }
}
