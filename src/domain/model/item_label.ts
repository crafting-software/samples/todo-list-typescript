
export class ItemLabel {
  public static of(value: string): ItemLabel {
    return new ItemLabel(value);
  }

  private constructor(public readonly value: string) {
  }
}
